import Vue from 'vue'
import router from './router'

export default {
  user: {
    authenticated: false,
    profile: null
  },

  _setToken (data) {
    localStorage.setItem('id_token', data.meta.token)
    Vue.http.headers.common['Authorization'] = 'Bearer ' + data.meta.token
    this.user.authenticated = true
    this.user.profile = data.data
  },

  _destroyToken () {
    localStorage.removeItem('id_token')
    this.user.authenticated = false
    this.user.profile = null
  },

  check () {
    if (localStorage.getItem('id_token') !== null) {
      Vue.http.get(
        'api/user',
      ).then(response => {
        console.log(response)
        this.user.authenticated = true
        this.user.profile = response.data.data
      })
    }
  },

  isAuthenticated () {
    return this.user.authenticated
  },

  register (context, user) {
    Vue.http.post(
      'api/register',
      {
        name: user.name,
        email: user.email,
        password: user.password
      }
    ).then(response => {
      console.log(response)
      context.success = true
    }, response => {
      console.log(response)
      context.response = response.data
      context.error = true
    })
  },

  login (context, credentials) {
    Vue.http.post(
      'api/signin',
      {
        email: credentials.email,
        password: credentials.password
      }
    ).then(response => {
      context.error = false
      this._setToken(response.data)
      router.push('account')
    }, response => {
      console.log(response)
      context.error = true
    })
  },

  logout () {
    this._destroyToken()
    router.push('/')
  }
}
